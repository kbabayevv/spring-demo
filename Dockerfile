FROM openjdk:17-alpine
WORKDIR /app
COPY build/libs/spring-demo-1.0.jar /spring-demo/
EXPOSE 8080
CMD ["java", "-jar", "/spring-demo/spring-demo-1.0.jar"]
